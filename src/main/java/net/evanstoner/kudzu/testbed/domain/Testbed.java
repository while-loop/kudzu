package net.evanstoner.kudzu.testbed.domain;

import com.fasterxml.jackson.annotation.*;
import net.evanstoner.kudzu.tenancy.domain.Project;
import net.evanstoner.kudzu.provider.domain.Provider;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@JsonRootName("testbed")
public class Testbed extends TestbedEntity {

    @NotBlank
    private String name;

    private String description;

    @ManyToOne
    @JoinColumn(name = "project", updatable = false, nullable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Project project ;

    @ManyToOne
    @JoinColumn(name = "provider", updatable = false, nullable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Provider provider;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "testbed")
    @JsonIdentityReference(alwaysAsId = true)
    private Collection<Host> hosts = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "testbed")
    @JsonIdentityReference(alwaysAsId = true)
    private Collection<Network> networks = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "testbed")
    @JsonIdentityReference(alwaysAsId = true)
    private Collection<Link> links = new ArrayList<>();

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "testbed")
    @JsonIdentityReference(alwaysAsId = true)
    private Router router;

    Testbed() {}

    public Testbed(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @JsonProperty("project")
    public void setProject(Long projectId) {
        this.project = new Project(projectId);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    @JsonProperty("provider")
    public void setProvider(Long providerId) {
        setProvider(new Provider(providerId));
    }

    public Collection<Network> getNetworks() {
        return networks;
    }

    public void setNetworks(Collection<Network> networks) {
        this.networks = networks;
    }

    public Collection<Host> getHosts() {
        return hosts;
    }

    public void setHosts(Collection<Host> hosts) {
        this.hosts = hosts;
    }

    public Collection<Link> getLinks() {
        return links;
    }

    public void setLinks(Collection<Link> links) {
        this.links = links;
    }

    public Router getRouter() {
        return router;
    }

    public void setRouter(Router router) {
        this.router = router;
    }
}
