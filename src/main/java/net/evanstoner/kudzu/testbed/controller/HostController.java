package net.evanstoner.kudzu.testbed.controller;

import net.evanstoner.spring.controller.GenericReadWriteController;
import net.evanstoner.kudzu.testbed.domain.Host;
import net.evanstoner.kudzu.testbed.service.HostService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/hosts")
public class HostController extends GenericReadWriteController<Host, HostService> {

    /**
     * Allow the client to query for hosts for a specific testbed only.
     * @param testbedId
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, params = "testbed")
    public Map getByTestbedId(@RequestParam("testbed") Long testbedId) {
        return wrapMany(this.service.findByTestbedId(testbedId));
    }

}
