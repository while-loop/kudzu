package net.evanstoner.kudzu.testbed.controller;

import net.evanstoner.spring.controller.GenericReadWriteController;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import net.evanstoner.kudzu.testbed.service.TestbedService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/testbeds")
public class TestbedController extends GenericReadWriteController<Testbed, TestbedService> {

}
