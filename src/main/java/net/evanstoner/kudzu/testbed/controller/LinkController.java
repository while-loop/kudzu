package net.evanstoner.kudzu.testbed.controller;

import net.evanstoner.spring.controller.GenericReadWriteController;
import net.evanstoner.kudzu.testbed.domain.Link;
import net.evanstoner.kudzu.testbed.service.LinkService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/links")
public class LinkController extends GenericReadWriteController<Link, LinkService> {

    /**
     * Allow the client to query for links for a specific testbed only.
     * @param testbedId
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, params = "testbed")
    public Map getByTestbedId(@RequestParam("testbed") Long testbedId) {
        return wrapMany(this.service.findByTestbedId(testbedId));
    }

}
