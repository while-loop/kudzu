package net.evanstoner.kudzu.testbed.repository;

import net.evanstoner.kudzu.testbed.domain.Host;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import org.springframework.data.repository.CrudRepository;

public interface HostRepository extends CrudRepository<Host, Long> {

    public Iterable<Host> findByTestbed(Testbed testbed);

    public Integer countByTestbed(Testbed testbed);

}
