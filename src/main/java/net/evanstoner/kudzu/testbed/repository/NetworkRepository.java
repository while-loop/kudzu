package net.evanstoner.kudzu.testbed.repository;

import net.evanstoner.kudzu.testbed.domain.Network;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import org.springframework.data.repository.CrudRepository;

public interface NetworkRepository extends CrudRepository<Network, Long> {

    public Iterable<Network> findByTestbed(Testbed testbed);

    public Network findOneByTestbedAndService(Testbed testbed, boolean service);

}
