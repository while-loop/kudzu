package net.evanstoner.kudzu.testbed.service;

import net.evanstoner.kudzu.testbed.domain.Network;
import net.evanstoner.kudzu.testbed.domain.Router;
import net.evanstoner.kudzu.testbed.domain.Testbed;
import net.evanstoner.kudzu.testbed.repository.TestbedRepository;
import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Transactional
@Service
public class TestbedServiceImpl extends GenericReadWriteServiceImpl<Testbed, TestbedRepository> implements TestbedService {

    @Autowired
    NetworkService networkService;
    @Autowired
    RouterService routerService;

    /**
     * Create a testbed and any supporting infrastructure (e.g. service network).
     * @param testbed
     * @return
     */
    @Override
    public Testbed create(Testbed testbed) {
        Testbed savedTestbed = update(testbed);
        Network serviceNetwork = networkService.createServiceNetworkForTestbed(savedTestbed);
        Router router = routerService.createRouterForTestbed(savedTestbed);
        // since the network and router own the relationship, we don't have to persist them as
        // attributes of the testbed; however, we do have to set them on the model so that the client
        // gets them back in attributes as expected
        savedTestbed.setNetworks(Collections.singletonList(serviceNetwork));
        savedTestbed.setRouter(router);
        return savedTestbed;
    }
}
