package net.evanstoner.kudzu.testbed.service;

import net.evanstoner.spring.service.GenericReadWriteService;
import net.evanstoner.kudzu.testbed.domain.Router;
import net.evanstoner.kudzu.testbed.domain.Testbed;

public interface RouterService extends GenericReadWriteService<Router> {

    Router createRouterForTestbed(Testbed testbed);

    Router findByTestbedId(Long testbedId);

}
