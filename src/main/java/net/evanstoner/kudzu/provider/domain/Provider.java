package net.evanstoner.kudzu.provider.domain;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonRootName;
import net.evanstoner.spring.entity.BaseEntity;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
@JsonRootName("provider")
public class Provider extends BaseEntity {

    @NotBlank
    private String name;

    @NotBlank
    @Column(updatable = false)
    private String testbedDriver;

    private String testbedConfig;

    @NotBlank
    @Column(updatable = false)
    private String routerDriver;

    private String routerConfig;

    @OneToMany(mappedBy = "provider")
    @JsonIdentityReference(alwaysAsId = true)
    private Collection<Image> images;

    @OneToMany(mappedBy = "provider")
    @JsonIdentityReference(alwaysAsId = true)
    private Collection<Flavor> flavors;

    Provider() {}

    public Provider(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTestbedDriver() {
        return testbedDriver;
    }

    public void setTestbedDriver(String testbedDriver) {
        this.testbedDriver = testbedDriver;
    }

    public Collection<Image> getImages() {
        return images;
    }

    public void setImages(Collection<Image> images) {
        this.images = images;
    }

    public Collection<Flavor> getFlavors() {
        return flavors;
    }

    public void setFlavors(Collection<Flavor> flavors) {
        this.flavors = flavors;
    }

    public String getRouterDriver() {
        return routerDriver;
    }

    public void setRouterDriver(String routerDriver) {
        this.routerDriver = routerDriver;
    }

    public String getTestbedConfig() {
        return testbedConfig;
    }

    public void setTestbedConfig(String testbedConfig) {
        this.testbedConfig = testbedConfig;
    }

    public String getRouterConfig() {
        return routerConfig;
    }

    public void setRouterConfig(String routerConfig) {
        this.routerConfig = routerConfig;
    }
}
