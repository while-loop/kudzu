package net.evanstoner.kudzu.provider.repository;

import net.evanstoner.kudzu.provider.domain.Provider;
import org.springframework.data.repository.CrudRepository;

public interface ProviderRepository extends CrudRepository<Provider, Long> {
}
