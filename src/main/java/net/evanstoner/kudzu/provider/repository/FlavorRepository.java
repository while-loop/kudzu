package net.evanstoner.kudzu.provider.repository;

import net.evanstoner.kudzu.provider.domain.Flavor;
import net.evanstoner.kudzu.provider.domain.Provider;
import org.springframework.data.repository.CrudRepository;

public interface FlavorRepository extends CrudRepository<Flavor, Long> {

    Iterable<Flavor> findByProvider(Provider provider);

}
