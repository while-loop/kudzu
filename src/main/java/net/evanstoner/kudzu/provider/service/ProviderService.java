package net.evanstoner.kudzu.provider.service;

import net.evanstoner.kudzu.provider.domain.Provider;
import net.evanstoner.spring.service.GenericReadWriteService;

public interface ProviderService extends GenericReadWriteService<Provider> {

}
