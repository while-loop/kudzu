package net.evanstoner.kudzu.provider.service;

import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import net.evanstoner.kudzu.provider.domain.Flavor;
import net.evanstoner.kudzu.provider.domain.Provider;
import net.evanstoner.kudzu.provider.repository.FlavorRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class FlavorServiceImpl extends GenericReadWriteServiceImpl<Flavor, FlavorRepository> implements FlavorService {

    @Override
    public Iterable<Flavor> findByProviderId(long providerId) {
        return repository.findByProvider(new Provider(providerId));
    }

}
