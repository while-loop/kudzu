package net.evanstoner.kudzu.core.service;

import net.evanstoner.kudzu.build.driver.RouterDriver;
import net.evanstoner.kudzu.build.driver.TestbedDriver;
import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.provision.driver.ProvisionDriver;
import net.evanstoner.kudzu.provision.driver.SourceDriver;

public interface DriverService {

    TestbedDriver loadTestbedDriver(String classname, String config) throws ReflectiveOperationException, ConfigurationException;

    RouterDriver loadRouterDriver(String classname, String config) throws ReflectiveOperationException, ConfigurationException;

    ProvisionDriver loadProvisionDriver(String classname, String config) throws ReflectiveOperationException, ConfigurationException;

    SourceDriver loadSourceDriver(String classname, String config) throws ReflectiveOperationException, ConfigurationException;

}
