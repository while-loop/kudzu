package net.evanstoner.kudzu.build.task;

import net.evanstoner.kudzu.build.BuildManager;
import net.evanstoner.kudzu.build.domain.Build;
import net.evanstoner.kudzu.build.domain.BuildTask;
import net.evanstoner.kudzu.build.domain.PersonaAssignmentInstance;
import net.evanstoner.kudzu.build.service.InstanceService;
import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.core.service.DriverService;
import net.evanstoner.kudzu.provision.domain.Provisioner;
import net.evanstoner.kudzu.provision.driver.ProvisionDriver;
import net.evanstoner.kudzu.provision.exception.ProvisionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class ProvisionBuildTask extends AbstractBuildTask {

    private static final Logger LOG = LoggerFactory.getLogger(ProvisionBuildTask.class);

    private final DriverService driverService;
    private final InstanceService instanceService;

    /** How many provisioners can be invoked in parallel when one priority contains multiple provisioners **/
    private ExecutorService provisionPool;

    private Map<Provisioner, ProvisionDriver> cachedProvisionDrivers = new HashMap<>();

    public ProvisionBuildTask(Build build, BuildManager buildManager, InstanceService instanceService,
                              DriverService driverService) {
        super(build, buildManager, BuildTask.PROVISIONING);
        this.instanceService = instanceService;
        this.driverService = driverService;
        //TODO: load parallel thread count from config
        provisionPool = Executors.newFixedThreadPool(5);
    }

    @Override
    protected void doTask() throws InterruptedException {
        LOG.info("provisioning " + build);

        // group all of the persona assignment instances by their base assignment's priority
        Map<Integer, List<PersonaAssignmentInstance>> priorityGroupedPAIs =
                build.getPersonaAssignmentInstances().stream()
                .collect(Collectors.groupingBy(pai -> pai.getBase().getPriority()));

        // sort the priorities so that they can be handled in the proper order
        List<Integer> sortedPriorities = priorityGroupedPAIs.keySet().stream()
                .sorted()
                .collect(Collectors.toList());

        // handle all priorities, ensuring all PAIs of one priority are completed before moving onto
        // the next set of PAIs
        for (Integer priority : sortedPriorities) {
            List<PersonaAssignmentInstance> paisForPriority = priorityGroupedPAIs.get(priority);
            LOG.debug(build + " started applying " + paisForPriority.size() + " persona assignment instances of priority " + priority);
            applyGroupedPAIs(paisForPriority);
            LOG.debug(build + " finished applying " + paisForPriority.size() + " persona assignment instances of priority " + priority);
        }
    }

    private void applyGroupedPAIs(List<PersonaAssignmentInstance> pais) throws InterruptedException {
        List<Callable<Object>> tasks = new ArrayList<>(pais.size());

        for (PersonaAssignmentInstance pai : pais) {
            tasks.add(Executors.callable(new ApplyPAITask(pai)));
        }

        provisionPool.invokeAll(tasks);
    }

    private class ApplyPAITask implements Runnable {

        private final PersonaAssignmentInstance pai;

        ApplyPAITask(PersonaAssignmentInstance pai) {
            this.pai = pai;
        }

        @Override
        public void run() {
            Provisioner provisioner = pai.getBase().getPersona().getProvisioner();

            try {
                ProvisionDriver driver = driverService.loadProvisionDriver(provisioner.getProvisionDriver(), provisioner.getProvisionConfig());
                String result = driver.provision(pai);
                instanceService.personaAssignmentInstanceService().notifyApplied(pai, result);
            } catch (ReflectiveOperationException | ConfigurationException | ProvisionException ex) {
                LOG.error("failed to apply persona assignment instance", ex);
                instanceService.notifyInstanceError(pai, ex.getMessage());
            }
        }

    }

}
