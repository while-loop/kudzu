package net.evanstoner.kudzu.build.task;

import net.evanstoner.kudzu.build.BuildManager;
import net.evanstoner.kudzu.build.domain.*;
import net.evanstoner.kudzu.build.driver.RouterDriver;
import net.evanstoner.kudzu.build.driver.TestbedDriver;
import net.evanstoner.kudzu.build.exception.BuildException;
import net.evanstoner.kudzu.build.exception.InvalidTaskChangeException;
import net.evanstoner.kudzu.build.service.InstanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InstantiateBuildTask extends AbstractBuildTask {

    private static final Logger LOG = LoggerFactory.getLogger(InstantiateBuildTask.class);

    private InstanceService instanceService;
    private TestbedDriver testbedDriver;
    private RouterDriver routerDriver;

    public InstantiateBuildTask(Build build, TestbedDriver testbedDriver, RouterDriver routerDriver,
                                BuildManager buildManager, InstanceService instanceService) {
        super(build ,buildManager, BuildTask.BUILDING);
        this.testbedDriver = testbedDriver;
        this.routerDriver = routerDriver;
        this.instanceService = instanceService;
    }

    @Override
    public void doTask() throws BuildException {
        LOG.info("instantiating " + build);

        // 1. build testbed: components for isolation, or components shared across other resources
        buildTestbed();
        // 2. build networks
        buildNetworks();
        // 3. build hosts: may require networks (specifically service) to be built
        buildHosts();
        // 4. build links: requires both hosts and networks to exist
        buildLinks();

        // 5. with all other resources built, we can built the router for establishing host access
        buildRouter();
        // 6. prepare the router in any way necessary, e.g. installing packages, creating keys, etc.
        prepareRouter();
        // 7. once the router is prepared, we can create access for each host
        createHostAccess();
    }

    private void buildTestbed() throws BuildException {
        TestbedInstance ti = build.getTestbedInstance();
        String ref = testbedDriver.buildTestbed(ti.getBase());
        instanceService.notifyInstanceActive(ti, ref);
    }

    private void buildNetworks() throws BuildException {
        Iterable<NetworkInstance> networkInstances = build.getNetworkInstances();
        for (NetworkInstance ni : networkInstances) {
            String ref = testbedDriver.buildNetwork(ni.getBase());
            instanceService.notifyInstanceActive(ni, ref);
        }
    }

    private void buildHosts() throws BuildException {
        Iterable<HostInstance> hostInstances = build.getHostInstances();
        for (HostInstance hi : hostInstances) {
            String ref = testbedDriver.buildHost(hi.getBase());
            instanceService.notifyInstanceActive(hi, ref);
        }
    }

    private void buildLinks() throws BuildException {
        Iterable<LinkInstance> networkInstances = build.getLinkInstances();
        for (LinkInstance li : networkInstances) {
            String ref = testbedDriver.buildLink(li.getBase());
            instanceService.notifyInstanceActive(li, ref);
        }
    }

    private void buildRouter() throws BuildException {
        RouterInstance ri = build.getRouterInstance();
        TestbedInstance ti = build.getTestbedInstance();
        String ref = testbedDriver.buildRouter(ri.getBase(), ti);
        instanceService.notifyInstanceActive(ri, ref);
    }

    private void prepareRouter() {
        RouterInstance ri = build.getRouterInstance();
        String[] access = routerDriver.configureRouterInstance(ri);
        instanceService.routerInstanceService().notifyRouterInstanceConfigured(ri, access[0], access[1]);
    }

    private void createHostAccess() {
        Iterable<HostInstance> hostInstances = build.getHostInstances();
        RouterInstance ri = build.getRouterInstance();
        for (HostInstance hi : hostInstances) {
            Object[] access = routerDriver.createHostInstanceAccess(hi, ri);
            instanceService.hostInstanceService().notifyHostInstanceAccessible(hi,
                    (String)access[0],
                    (Integer)access[1]);
        }
    }

}
