package net.evanstoner.kudzu.build.task;

import net.evanstoner.kudzu.build.BuildManager;
import net.evanstoner.kudzu.build.domain.Build;
import net.evanstoner.kudzu.build.domain.BuildTask;
import net.evanstoner.kudzu.build.exception.InvalidTaskChangeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class AbstractBuildTask implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractBuildTask.class);

    protected Build build;
    protected final BuildManager buildManager;
    protected final BuildTask buildTask;

    AbstractBuildTask(Build build, BuildManager buildManager, BuildTask task) {
        this.build = build;
        this.buildManager = buildManager;
        this.buildTask = task;
    }

    abstract protected void doTask() throws Exception;

    @Override
    public void run() {
        try {
            build = buildManager.notifyBuildTaskStarted(build, buildTask);
        } catch (InvalidTaskChangeException e) {
            LOG.error("failed to start task on " + build, e);
            buildManager.notifyBuildTaskFailed(build, buildTask, "failed to change task");
            return;
        }

        try {
            doTask();
        } catch (Throwable t) {
            LOG.error(buildTask + " failed on " + build, t);
            buildManager.notifyBuildTaskFailed(build, buildTask, t);
            return;
        }

        try {
            build = buildManager.notifyBuildTaskEnded(build, buildTask);
        } catch (InvalidTaskChangeException e) {
            LOG.error("failed to end task on " + build, e);
        }
    }
}
