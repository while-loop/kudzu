package net.evanstoner.kudzu.build.domain;

public enum BuildTask {

    /** The build and its instances are being built. **/
    BUILDING,

    /** The build's instances are being provisioned. **/
    PROVISIONING,

    /** The build is being destroyed. **/
    DESTROYING

}
