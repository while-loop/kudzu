package net.evanstoner.kudzu.build.domain;

import com.fasterxml.jackson.annotation.JsonRootName;
import net.evanstoner.kudzu.testbed.domain.Link;

import javax.persistence.Entity;

@Entity
@JsonRootName("linkInstance")
public class LinkInstance extends Instance<Link> {
    public LinkInstance() {
    }

    public LinkInstance(Link base, Build build) {
        super(base, build);
    }
}
