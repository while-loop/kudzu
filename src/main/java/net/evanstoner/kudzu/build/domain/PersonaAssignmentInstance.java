package net.evanstoner.kudzu.build.domain;

import com.fasterxml.jackson.annotation.JsonRootName;
import net.evanstoner.kudzu.provision.domain.PersonaAssignment;

import javax.persistence.Entity;

@Entity
@JsonRootName("personaAssignmentInstance")
public class PersonaAssignmentInstance extends Instance<PersonaAssignment> {

    private String overrideConfig;

    private boolean enabled = true;

    private String result;

    public PersonaAssignmentInstance() {
        super();
    }

    public PersonaAssignmentInstance(PersonaAssignment base, Build build) {
        super(base, build);
    }

    public String getOverrideConfig() {
        return overrideConfig;
    }

    public void setOverrideConfig(String overrideConfig) {
        this.overrideConfig = overrideConfig;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
