package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.NetworkInstance;

public interface NetworkInstanceService extends BaseInstanceService<NetworkInstance> {
}
