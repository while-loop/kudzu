package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.LinkInstance;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class LinkInstanceServiceImpl
        extends BaseInstanceServiceImpl<LinkInstance>
        implements LinkInstanceService {

}
