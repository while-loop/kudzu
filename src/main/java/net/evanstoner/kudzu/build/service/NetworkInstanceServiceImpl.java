package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.NetworkInstance;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class NetworkInstanceServiceImpl
        extends BaseInstanceServiceImpl<NetworkInstance>
        implements NetworkInstanceService {

}
