package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.RouterInstance;

public interface RouterInstanceService extends BaseInstanceService<RouterInstance> {

    void notifyRouterInstanceConfigured(RouterInstance routerInstance, String externalIpAddress, String accessDetails);

    void notifyRouterInstanceConfigurationError(RouterInstance routerInstance, String error);

    void notifyRouterInstanceDeconfigured(RouterInstance routerInstance);

}
