package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.RouterInstance;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class RouterInstanceServiceImpl
        extends BaseInstanceServiceImpl<RouterInstance>
        implements RouterInstanceService {

    private Logger LOG = LogManager.getLogger(RouterInstanceServiceImpl.class);

    @Override
    public void notifyRouterInstanceConfigured(RouterInstance routerInstance, String externalIpAddress, String accessDetails) {
        routerInstance.setConfigured(true);
        routerInstance.setExternalIpAddress(externalIpAddress);
        routerInstance.setAccessDetails(accessDetails);

        repository.save(routerInstance);

        LOG.info(String.format("%s is configured (external ip address: %s; access details: %s)",
                routerInstance,
                routerInstance.getExternalIpAddress(),
                routerInstance.getAccessDetails()));
    }

    @Override
    public void notifyRouterInstanceConfigurationError(RouterInstance routerInstance, String error) {
        routerInstance.setConfigured(false);
        routerInstance.setError(error);

        repository.save(routerInstance);

        LOG.warn(routerInstance + " failed configuration: " + error);
    }

    @Override
    public void notifyRouterInstanceDeconfigured(RouterInstance routerInstance) {
        routerInstance.setConfigured(false);

        repository.save(routerInstance);

        LOG.info(routerInstance + " is deconfigured");
    }

}
