package net.evanstoner.kudzu.build.service;

import net.evanstoner.kudzu.build.domain.PersonaAssignmentInstance;

public interface PersonaAssignmentInstanceService extends BaseInstanceService<PersonaAssignmentInstance> {

    public void notifyApplied(PersonaAssignmentInstance pai, String result);

}
