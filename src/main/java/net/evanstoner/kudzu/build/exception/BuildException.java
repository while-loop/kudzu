package net.evanstoner.kudzu.build.exception;

public class BuildException extends Exception {

    public BuildException(String message) {
        super(message);
    }

    public BuildException(String message, Exception exception) {
        super(message, exception);
    }

    public BuildException(Exception exception) {
        super(exception);
    }

}
