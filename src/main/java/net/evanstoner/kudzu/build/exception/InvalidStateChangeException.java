package net.evanstoner.kudzu.build.exception;

import net.evanstoner.kudzu.build.domain.BuildState;

public class InvalidStateChangeException extends Exception {

    public InvalidStateChangeException(BuildState fromState, BuildState toState) {
        this(fromState, toState, null);
    }

    public InvalidStateChangeException(BuildState fromState, BuildState toState, String reason) {
        super("Cannot transition build from " + fromState
                + " to " + toState
                + (reason != null ? ": " + reason : ""));
    }

}
