package net.evanstoner.kudzu.build.controller;

import net.evanstoner.kudzu.build.domain.RouterInstance;
import net.evanstoner.kudzu.build.service.RouterInstanceService;
import net.evanstoner.spring.controller.GenericReadOnlyController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("routerInstances")
public class RouterInstanceController extends GenericReadOnlyController<RouterInstance, RouterInstanceService> {

    @RequestMapping(method = RequestMethod.GET, params = "build")
    public Map getByBuildId(@RequestParam("build") Long buildId) {
        return wrapMany(this.service.findByBuildId(buildId));
    }

}
