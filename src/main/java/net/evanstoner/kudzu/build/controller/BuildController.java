package net.evanstoner.kudzu.build.controller;

import net.evanstoner.kudzu.build.domain.Build;
import net.evanstoner.kudzu.build.service.BuildService;
import net.evanstoner.spring.controller.GenericReadWriteController;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/builds")
public class BuildController extends GenericReadWriteController<Build, BuildService> {

    @RequestMapping(method = RequestMethod.GET, params = "testbed")
    public Map getByTestbedId(@RequestParam("testbed") Long testbedId) {
        return wrapMany(this.service.findByTestbedId(testbedId));
    }

}
