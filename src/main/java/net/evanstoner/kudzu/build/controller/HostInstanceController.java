package net.evanstoner.kudzu.build.controller;

import net.evanstoner.kudzu.build.domain.HostInstance;
import net.evanstoner.kudzu.build.service.HostInstanceService;
import net.evanstoner.spring.controller.GenericReadOnlyController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/hostInstances")
public class HostInstanceController extends GenericReadOnlyController<HostInstance, HostInstanceService> {

    @RequestMapping(method = RequestMethod.GET, params = "build")
    public Map getByBuildId(@RequestParam("build") Long buildId) {
        return wrapMany(this.service.findByBuildId(buildId));
    }

}
