package net.evanstoner.kudzu.build.repository;

import net.evanstoner.kudzu.build.domain.LinkInstance;

public interface LinkInstanceRepository extends BaseInstanceRepository<LinkInstance> {
}
