package net.evanstoner.kudzu.build.repository;

import net.evanstoner.kudzu.build.domain.TestbedInstance;

public interface TestbedInstanceRepository extends BaseInstanceRepository<TestbedInstance> {

}
