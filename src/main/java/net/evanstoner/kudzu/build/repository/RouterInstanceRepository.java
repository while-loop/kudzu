package net.evanstoner.kudzu.build.repository;

import net.evanstoner.kudzu.build.domain.RouterInstance;

public interface RouterInstanceRepository extends BaseInstanceRepository<RouterInstance> {
}
