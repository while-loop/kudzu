package net.evanstoner.kudzu.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidIpAddressException extends RuntimeException {

    public InvalidIpAddressException(String ipAddress) {
        super(ipAddress + " is not a valid IP address");
    }

}
