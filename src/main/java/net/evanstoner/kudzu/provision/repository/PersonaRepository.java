package net.evanstoner.kudzu.provision.repository;

import net.evanstoner.kudzu.provision.domain.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long> {
}
