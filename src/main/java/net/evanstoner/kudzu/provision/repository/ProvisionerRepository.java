package net.evanstoner.kudzu.provision.repository;

import net.evanstoner.kudzu.provision.domain.Provisioner;
import org.springframework.data.repository.CrudRepository;

public interface ProvisionerRepository extends CrudRepository<Provisioner, Long> {
}
