package net.evanstoner.kudzu.provision.driver;

import net.evanstoner.kudzu.core.exception.ConfigurationException;

import java.io.IOException;

public abstract class SourceDriver {

    public abstract void configure(String config) throws ConfigurationException;

    public abstract void retrieve(String source) throws IOException;

}
