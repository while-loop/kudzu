package net.evanstoner.kudzu.provision.driver;

import net.evanstoner.kudzu.build.domain.PersonaAssignmentInstance;
import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.provision.exception.ProvisionException;

public abstract class ProvisionDriver {

    public enum ProvisionFeature {

    }

    public abstract void configure(String config) throws ConfigurationException;

    /**
     * Indicates whether this driver supports the specified feature.
     * @param feature
     * @return
     */
    public abstract boolean supports(ProvisionFeature feature);

    /**
     * Realize the PersonaAssignmentInstance with this driver, i.e. provision the HostInstance of
     * the given PersonaAssignmentInstance with the referenced PersonaAssignment.
     * @param pai
     * @return Output from the provisioning process, if applicable.
     */
    public abstract String provision(PersonaAssignmentInstance pai) throws ProvisionException;

}
