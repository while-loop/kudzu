package net.evanstoner.kudzu.provision.domain;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import net.evanstoner.kudzu.testbed.domain.Host;
import net.evanstoner.kudzu.testbed.domain.TestbedEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
@JsonRootName("personaAssignment")
public class PersonaAssignment extends TestbedEntity {

    @NotNull
    private Integer priority = 50;

    private String config;

    @ManyToOne
    @JoinColumn(name = "persona", nullable = false, updatable = true)
    @JsonIdentityReference(alwaysAsId = true)
    private Persona persona;

    @ManyToOne
    @JoinColumn(name = "host", nullable = false, updatable = false)
    @JsonIdentityReference(alwaysAsId = true)
    private Host host;

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @JsonProperty("persona")
    public void setPersona(Long personaId) {
        this.persona = new Persona(personaId);
    }

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    @JsonProperty("host")
    public void setHost(Long hostId) {
        this.host = new Host(hostId);
    }
}
