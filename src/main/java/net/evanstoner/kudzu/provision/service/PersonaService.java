package net.evanstoner.kudzu.provision.service;

import net.evanstoner.spring.service.GenericReadWriteService;
import net.evanstoner.kudzu.provision.domain.Persona;

public interface PersonaService extends GenericReadWriteService<Persona> {
}
