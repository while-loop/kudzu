package net.evanstoner.kudzu.provision.service;

import net.evanstoner.spring.service.GenericReadWriteServiceImpl;
import net.evanstoner.kudzu.provision.domain.Persona;
import net.evanstoner.kudzu.provision.repository.PersonaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class PersonaServiceImpl
        extends GenericReadWriteServiceImpl<Persona, PersonaRepository>
        implements PersonaService {
}
