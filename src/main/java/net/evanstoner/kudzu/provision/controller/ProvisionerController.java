package net.evanstoner.kudzu.provision.controller;

import net.evanstoner.spring.controller.GenericReadWriteController;
import net.evanstoner.kudzu.provision.domain.Provisioner;
import net.evanstoner.kudzu.provision.service.ProvisionerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/provisioners")
public class ProvisionerController extends GenericReadWriteController<Provisioner, ProvisionerService> {
}
