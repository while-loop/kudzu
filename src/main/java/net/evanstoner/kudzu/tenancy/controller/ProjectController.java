package net.evanstoner.kudzu.tenancy.controller;

import net.evanstoner.spring.controller.GenericReadWriteController;
import net.evanstoner.kudzu.tenancy.domain.Project;
import net.evanstoner.kudzu.tenancy.service.ProjectService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/projects")
public class ProjectController extends GenericReadWriteController<Project, ProjectService> {
}
