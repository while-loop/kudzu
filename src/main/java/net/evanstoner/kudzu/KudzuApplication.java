package net.evanstoner.kudzu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class KudzuApplication {

    public static void main(String[] args) {
        SpringApplication.run(KudzuApplication.class, args);
    }
}
