package net.evanstoner.kudzu.driver.router;

import net.evanstoner.kudzu.build.domain.HostInstance;
import net.evanstoner.kudzu.build.domain.RouterInstance;
import net.evanstoner.kudzu.build.domain.TestbedInstance;
import net.evanstoner.kudzu.build.driver.RouterDriver;
import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.testbed.domain.RouterType;

import java.util.UUID;

public class NoopRouterDriver extends RouterDriver {

    @Override
    public void configure(String config) throws ConfigurationException {

    }

    @Override
    public RouterType getType() {
        return RouterType.FLAT;
    }

    @Override
    public String[] configureRouterInstance(RouterInstance routerInstance) {
        return new String[] {null, null};
    }

    @Override
    public void deconfigureRouterInstance(RouterInstance routerInstance) {
        // nothing to do
    }

    @Override
    public String[] createHostInstanceAccess(HostInstance hostInstance, RouterInstance routerInstance) {
        return new String[] {hostInstance.getBase().getServiceLink().getIpAddress(), null};
    }

    @Override
    public void destroyHostInstanceAccess(HostInstance hostInstance, RouterInstance routerInstance) {
        // nothing to do
    }

}
