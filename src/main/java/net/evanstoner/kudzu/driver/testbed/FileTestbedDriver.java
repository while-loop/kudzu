package net.evanstoner.kudzu.driver.testbed;

import net.evanstoner.kudzu.build.domain.*;
import net.evanstoner.kudzu.build.driver.TestbedDriver;
import net.evanstoner.kudzu.build.exception.BuildException;
import net.evanstoner.kudzu.build.service.BuildService;
import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.testbed.domain.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Builds a testbed as a collection of files following the structure below:
 * testbeds/
 *   :testbedId/
 *     hosts/
 *       :hostId
 *     links/
 *       :linkId
 *     networks/
 *       :networkId
 *     router-:routerId
 */
public class FileTestbedDriver extends TestbedDriver {

    private String rootPath = ".";

    private static final String TESTBEDS_DIR = "testbeds";
    private static final String HOSTS_DIR = "hosts";
    private static final String NETWORKS_DIR = "networks";
    private static final String LINKS_DIR = "links";

    public FileTestbedDriver(BuildService buildService) {
        super(buildService);
    }

    @Override
    public void configure(String config) throws ConfigurationException {
        rootPath = config;

        File file = new File(rootPath);
        if (!file.exists() || !file.isDirectory()) {
            throw new ConfigurationException("path doesn't exist or is not a directory: " + rootPath);
        }
    }

    @Override
    public String buildTestbed(Testbed testbed) throws BuildException {
        Path path = testbedPath(testbed);
        try {
            Files.createDirectories(path);
            Files.createDirectory(Paths.get(path.toString(), HOSTS_DIR));
            Files.createDirectory(Paths.get(path.toString(), NETWORKS_DIR));
            Files.createDirectory(Paths.get(path.toString(), LINKS_DIR));
        } catch (IOException e) {
            throw new BuildException("cannot create testbed directory " + path + " or its children", e);
        }
        return path.toString();
    }

    @Override
    public void destroyTestbedInstance(TestbedInstance testbedInstance) throws BuildException {
        Path path = testbedPath(testbedInstance.getBase());
        try {
            Files.delete(Paths.get(path.toString(), HOSTS_DIR));
            Files.delete(Paths.get(path.toString(), NETWORKS_DIR));
            Files.delete(Paths.get(path.toString(), LINKS_DIR));
            Files.delete(path);
        } catch (IOException e) {
            throw new BuildException("cannot delete testbed directory " + path + " or its children", e);
        }
    }

    @Override
    public String buildNetwork(Network network) throws BuildException {
        Path path = networkPath(network);
        try {
            Files.write(path, (network.getCidr() + "\n").getBytes());
        } catch (IOException e) {
            throw new BuildException("cannot create network file " + path, e);
        }
        return path.toString();
    }

    @Override
    public void destroyNetworkInstance(NetworkInstance networkInstance) throws BuildException {
        Path path = networkPath(networkInstance.getBase());
        try {
            Files.delete(path);
        } catch (IOException e) {
            throw new BuildException("cannot delete network file " + path, e);
        }
    }

    @Override
    public String buildHost(Host host) throws BuildException {
        Path path = hostPath(host);
        try {
            Files.write(path, (host.getName() + "\n").getBytes());
        } catch (IOException e) {
            throw new BuildException("cannot create host file " + path, e);
        }
        return path.toString();
    }

    @Override
    public void destroyHostInstance(HostInstance hostInstance) throws BuildException {
        Path path = hostPath(hostInstance.getBase());
        try {
            Files.delete(path);
        } catch (IOException e) {
            throw new BuildException("cannot delete host file " + path, e);
        }
    }

    @Override
    public String buildLink(Link link) throws BuildException {
        Path path = linkPath(link);
        try {
            Files.write(path, (link.getIpAddress() + "\n").getBytes());
        } catch (IOException e) {
            throw new BuildException("cannot create link file " + path, e);
        }
        return path.toString();
    }

    @Override
    public void destroyLinkInstance(LinkInstance linkInstance) throws BuildException {
        Path path = linkPath(linkInstance.getBase());
        try {
            Files.delete(path);
        } catch (IOException e) {
            throw new BuildException("cannot delete link file " + path, e);
        }
    }

    @Override
    public String buildRouter(Router router, TestbedInstance testbedInstance) throws BuildException {
        Path path = routerPath(router);
        try {
            Files.createFile(path);
        } catch (IOException e) {
            throw new BuildException("cannot create router file " + path, e);
        }
        return path.toString();
    }

    @Override
    public void destroyRouterInstance(RouterInstance routerInstance) throws BuildException {
        Path path = routerPath(routerInstance.getBase());
        try {
            Files.delete(path);
        } catch (IOException e) {
            throw new BuildException("cannot delete router file " + path, e);
        }
    }

    private Path testbedPath(Testbed testbed) {
        return Paths.get(rootPath,
                TESTBEDS_DIR,
                "" + testbed.getId());
    }

    private Path hostPath(Host host) {
        return Paths.get(testbedPath(host.getTestbed()).toString(),
                HOSTS_DIR,
                "" + host.getId());
    }

    private Path networkPath(Network network) {
        return Paths.get(testbedPath(network.getTestbed()).toString(),
                NETWORKS_DIR,
                "" + network.getId());
    }

    private Path linkPath(Link link) {
        return Paths.get(testbedPath(link.getTestbed()).toString(),
                LINKS_DIR,
                "" + link.getId());
    }

    private Path routerPath(Router router) {
        return Paths.get(testbedPath(router.getTestbed()).toString(),
                "router-" + router.getId());
    }

}
