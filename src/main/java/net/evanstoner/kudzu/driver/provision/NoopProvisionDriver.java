package net.evanstoner.kudzu.driver.provision;

import net.evanstoner.kudzu.build.domain.PersonaAssignmentInstance;
import net.evanstoner.kudzu.core.exception.ConfigurationException;
import net.evanstoner.kudzu.provision.driver.ProvisionDriver;
import net.evanstoner.kudzu.provision.exception.ProvisionException;

public class NoopProvisionDriver extends ProvisionDriver {

    @Override
    public void configure(String config) throws ConfigurationException {

    }

    @Override
    public boolean supports(ProvisionFeature feature) {
        return false;
    }

    @Override
    public String provision(PersonaAssignmentInstance pai) throws ProvisionException {
        return "noop";
    }
}
