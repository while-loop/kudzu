# Kudzu

_Kudzu is an IaaS and automation abstraction layer for cyber experiments._

## Running

1. `mvn spring-boot:run`
2. `http://localhost:8080`
